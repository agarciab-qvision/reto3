#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Simular ahorros
  Simular ahorros de acuerdo a plan de ahorros

  @CasoExitoso
  Scenario: Title of your scenario
    Given Ingresar a la opcion necesidades
    Given Ingresar a la opcion Estudio
    And Ingresar a la opcion Cuenta Ahorrador
    When Ingresar al simulador de Ahorro Formulario PasoUno
    And Ingresar al simulador de Ahorro Formulario PasoDos
    And Ingresar al simulador de Ahorro Formulario PasoTres
    Then Presentar por consola el valor que se debe ahorrar mensualmente

#  @tag2
# Scenario Outline: Title of your scenario outline
#   Given I want to write a step with <name>
#    When I check for the <value> in step
#    Then I verify the <status> in step

#    Examples: 
#      | name  | value | status  |
#      | name1 |     5 | success |
#      | name2 |     7 | Fail    |
