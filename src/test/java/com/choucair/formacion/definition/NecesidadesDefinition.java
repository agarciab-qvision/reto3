package com.choucair.formacion.definition;

import com.choucair.formacion.steps.NecesidadesSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class NecesidadesDefinition {

	@Steps
	NecesidadesSteps necesidadesSteps;
	
	@Given("^Ingresar a la opcion necesidades$")
	public void ingresar_a_la_opcion_necesidades() {
		
		necesidadesSteps.ingresoNecesidades();
		
	}

	@Given("^Ingresar a la opcion Estudio$")
	public void ingresar_a_la_opcion_Estudio() {
		
		necesidadesSteps.ingresoEstudio();
		
	}

	@Given("^Ingresar a la opcion Cuenta Ahorrador$")
	public void ingresar_a_la_opcion_Cuenta_Ahorrador() {
		
		necesidadesSteps.simulaTusAhorros();
	}

	@When("^Ingresar al simulador de Ahorro Formulario PasoUno$")
	public void ingresar_al_simulador_de_Ahorro_Formulario_PasoUno() {
		
		necesidadesSteps.diligenciarFormularioPasoUno();
	}


	@When("^Ingresar al simulador de Ahorro Formulario PasoDos$")
	public void ingresar_al_simulador_de_Ahorro_Formulario_PasoDos() {
		
		necesidadesSteps.diligenciarFormularioPasoDos();
		
	}

	@When("^Ingresar al simulador de Ahorro Formulario PasoTres$")
	public void ingresar_al_simulador_de_Ahorro_Formulario_PasoTres() {

		necesidadesSteps.visualizarPlanAhorro();
	}

	@Then("^Presentar por consola el valor que se debe ahorrar mensualmente$")
	public void presentar_por_consola_el_valor_que_se_debe_ahorrar_mensualmente() {

	}
}
