package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString; 

@DefaultUrl("https://www.grupobancolombia.com/wps/portal/personas")

public class NecesidadesPage extends PageObject {

	// Objetos Necesidades
	
	@FindBy(xpath="//*[@id=\'main-menu\']/div[2]/ul[1]/li[2]/a")
	public WebElementFacade btnNecesidades;
	
	@FindBy(xpath="//*[@id=\'necesidadesPersonas\']/div/div[1]/div[1]/h2")
	public WebElementFacade lblNecesidades;
	
	
	// Objetos Estudio
	
	@FindBy(xpath = "//*[@id=\'necesidadesPersonas\']/div/div[1]/div[1]/div/div[3]/div/a")
	public WebElementFacade btnEstudio;
	
	@FindBy(xpath="//*[@id=\'main-content\']/div[1]/div/div[2]/div/div/h1")
	public WebElementFacade lblEstudio;
	
	// Objetos Simulador Ahorros
	
	@FindBy(xpath = "//*[@id=\'wizard1\']/div/div[1]/div/div/div[1]/p[2]/strong/a")
	public WebElementFacade btnSimulaTusAhorros;
	
	@FindBy(xpath="//*[@id=\'sim-description\']/div[1]/h1")
	public WebElementFacade lblSimulaTusAhorros;
	

	// Objetos Formulario PasoUno
	
	@FindBy(xpath="//*[@id=\'browser-Off\']/div/div/div[1]/form/div[2]/select")
	public WebElementFacade lstParaQue;
	
	@FindBy(xpath="//*[@id=\'browser-Off\']/div/div/div[1]/form/div[3]/input")
	public WebElementFacade txtMeses;
	
	@FindBy(xpath="//*[@id=\'browser-Off\']/div/div/div[1]/form/div[4]/select")
	public WebElementFacade lstProducto;
	
	@FindBy(xpath="//*[@id=\'browser-Off\']/div/div/div[1]/form/div[5]/input")
	public WebElementFacade txtCuantoDinero;
	
	@FindBy(xpath="//*[@id=\'browser-Off\']/div/div/div[1]/form/div[8]/button")
	public WebElementFacade btnAgregarPlanDeAhorros;
	
	@FindBy(xpath="//*[@id=\'browser-Off\']/div/div/div[1]/form/div[9]/table/tbody/tr[1]/th[1]")
	public WebElementFacade lblSueno;
	
	// Objetos Formulario PasoDos
	
	
	@FindBy(xpath="//*[@id=\'browser-Off\']/div/ul/li[2]/a/div[2]/div/p")
	public WebElementFacade btnPasoDos;
			
	@FindBy(xpath="//*[@id=\'browser-Off\']/div/ul/li[2]/a/div[2]/div/p")
	public WebElementFacade lblDisposicionesLegales;
	
	@FindBy(xpath="//*[@id=\"browser-Off\"]/div/div/div[2]/div[3]/form/input")
	public WebElementFacade chkBxDisposicionesLegales;
	
	@FindBy(xpath="//*[@id=\'browser-Off\']/div/div/div[2]/div[3]/form/button")
	public WebElementFacade btnCalcularAhorro;
	

	@FindBy(xpath="//*[@id=\'browser-Off\']/div/ul/li[3]/a/div[2]/div/p")
	public WebElementFacade btnPasoTres;
	
	@FindBy(xpath="//*[@id=\'tablaAhorro\']/h3")
	public WebElementFacade lblResultadoSimulacion;
	
	@FindBy(xpath="//*[@id=\'tablaAhorro\']/table/tbody/tr[2]/td[2]")
	public WebElementFacade lblAhorroMensual;
	
	
	// Metodo Necesidades
	
	public void VerificarIngresoNecesidades() {
		btnNecesidades.click();
		String lblVerificacion = "Necesidades";
		String strMensaje = lblNecesidades.getText();
		assertThat(strMensaje, containsString(lblVerificacion));
				
	}

	public void VerificarIngresoEstudio() {
		btnEstudio.click();
		String lblVerificacion = "Estudia lo que quieres";
		String strMensaje = lblEstudio.getText();
		assertThat(strMensaje, containsString(lblVerificacion));
				
		
	}

	public void VerificarIngresoSimulaTusAhorros() {
		btnSimulaTusAhorros.click();
		String lblVerificacion = "Bienvenidos al Simulador de Ahorro e Inversión";
		String strMensaje = lblSimulaTusAhorros.getText();
		assertThat(strMensaje, containsString(lblVerificacion));
		
	}

	public void DiligenciarDatosPasoUno() {
		lstParaQue.selectByVisibleText("Viaje");
		txtMeses.sendKeys("9");
		lstProducto.selectByVisibleText("Fiducuenta");
		txtCuantoDinero.sendKeys("2000000");

	}

	public void VerificarDiligenciarDatosPasoUno() {
		btnAgregarPlanDeAhorros.click();
		String lblVerificacion = "Sueño";
		String strMensaje = lblSueno.getText();
		assertThat(strMensaje, containsString(lblVerificacion));	

	}

	
	public void VerificarDisposicionesLegalesPasoDos() {
		btnPasoDos.click();
		String lblVerificacion = "Disposiciones legales";
		String strMensaje = lblDisposicionesLegales.getText();
		assertThat(strMensaje, containsString(lblVerificacion));
		
	}
	
	public void ActivarCheckBox() {
		chkBxDisposicionesLegales.click();
		btnCalcularAhorro.click();
		
	}

	public void VerificarPlanAhorro() {
		btnPasoTres.click();
		String lblVerificacion = "Resultado de la simulación";
		String strMensaje = lblResultadoSimulacion.getText();
		assertThat(strMensaje, containsString(lblVerificacion));
		
	}

	public void ConsolaMostrarAhorroMensual() {
		
		String MostrarAhorroMensual = lblAhorroMensual.getText();
		System.out.println("El ahorro mensual debe ser de: " + MostrarAhorroMensual);
		
	}
	
}
