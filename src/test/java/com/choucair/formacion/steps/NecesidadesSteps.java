package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.NecesidadesPage;

import net.thucydides.core.annotations.Step;

public class NecesidadesSteps {

	NecesidadesPage necesidadesPage;
	 
	@Step
	public void ingresoNecesidades() {
			
		necesidadesPage.open();
		necesidadesPage.VerificarIngresoNecesidades();
		
	}

	public void ingresoEstudio() {
		
		necesidadesPage.VerificarIngresoEstudio();
		
	}

	public void simulaTusAhorros() {

		necesidadesPage.VerificarIngresoSimulaTusAhorros();
		
	}

	public void diligenciarFormularioPasoUno() {

		necesidadesPage.DiligenciarDatosPasoUno();
		necesidadesPage.VerificarDiligenciarDatosPasoUno();
		
	}

	public void diligenciarFormularioPasoDos() {
		necesidadesPage.VerificarDisposicionesLegalesPasoDos();
		necesidadesPage.ActivarCheckBox();
		
	}

	public void visualizarPlanAhorro() {
		necesidadesPage.VerificarPlanAhorro();
		necesidadesPage.ConsolaMostrarAhorroMensual();
		
	}
}
